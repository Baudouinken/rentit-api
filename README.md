# RentIT-backend API

## Describtion
This project is like a shopping App but it not possible to buy, only to rent IT Articles. 

Features:
 - Admin:
   - CRUD operations on Articles

 - User:
   - Rent and return articles
   - Extend the return Date of rented articles 


## Frontend
The frontend is an Angular project.

Repos Link: https://gitlab.com/Baudouinken/rentit-ui


## Deploy
The App is doplyed on a linux root-server with gitlab-runners through a [gitlab.yml](https://gitlab.com/Baudouinken/rentit-api/-/blob/for_gitlab/.gitlab-ci.yml) file
